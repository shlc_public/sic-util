<!-- Do not edit this file. It is automatically generated by API Documenter. -->

[Home](./index.md) &gt; [sic-util](./sic-util.md) &gt; [val\_empty](./sic-util.val_empty.md)

## val\_empty() function

函数"val\_space"检查 TypeScript 中的给定值是否为空。

**Signature:**

```typescript
val_empty: (value?: any) => boolean
```

## Parameters

<table><thead><tr><th>

Parameter


</th><th>

Type


</th><th>

Description


</th></tr></thead>
<tbody><tr><td>

value


</td><td>

any


</td><td>

_(Optional)_ "val\_space" 函数中的 "value" 参数是可选的，可以是任何数据类型。


</td></tr>
</tbody></table>
**Returns:**

boolean

函数 "val\_space" 返回一个布尔值，指示输入的 "value" 头尾是否有空格。

