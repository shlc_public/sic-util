## API Report File for "sic-util"

> Do not edit this file. It is a report generated by [API Extractor](https://api-extractor.com/).

```ts

import dayjs from 'dayjs';

// @public
export const author_passwordCheck: (str: string) => number;

// Warning: (ae-forgotten-export) The symbol "MenuInformation" needs to be exported by the entry point index.d.ts
//
// @public
export const author_router_add: (origin: MenuInformation[], local: MenuInformation[]) => ({
    resourcelist: ResourceList[] | undefined;
    menuid?: number | undefined;
    menuId?: number | undefined;
    parentid?: number | undefined;
    parentId?: number | undefined;
    parentname?: string | undefined;
    parentName?: string | undefined;
    resourceList?: ResourceList[] | undefined;
    sort?: number | undefined;
    label?: string | undefined;
    key?: string | undefined;
    icon?: any;
    element?: any;
    children?: any;
} | null)[];

// @public
export const author_router_filter: (origin: MenuInformation[], local: MenuInformation[]) => ({
    key: string | undefined;
    menuid: number | undefined;
    label: string | undefined;
    resourcelist: ResourceList[] | undefined;
    children: any;
} | null)[];

// Warning: (ae-forgotten-export) The symbol "ResourceItem" needs to be exported by the entry point index.d.ts
//
// @public
export const author_strict: (list: ResourceItem[], id: number) => boolean;

// @public
export const author_traceId: (origin: string, url: string) => string;

// @public
export const city_code_text: (provinceId: string, cityId?: string, districtId?: string) => string;

// @public
export const city_options: (level?: string) => ({
    label: string;
    value: string;
    children?: undefined;
} | {
    label: string;
    value: string;
    children: {
        label: string;
        value: string;
    }[];
})[];

// @public
export const city_two_code_text: (provinceId: string, cityId?: string) => string;

// @public
export const demo: () => boolean;

// @public
export const file_calculate_md5: (file: any) => Promise<unknown>;

// Warning: (ae-forgotten-export) The symbol "IProps" needs to be exported by the entry point index.d.ts
//
// @public
export const file_load: (props: IProps) => Promise<void> | undefined;

// @public
export const file_open: (props: IProps) => void;

// @public
export const get_multiple_color: (value: string | null, opacity: number) => string;

// @public
export const getOptionConfig: (value: number, array: any[]) => any;

// @public
export const getUrlConfig: (url: string | null) => {
    fileName: string;
    prefix: string;
    suffix: string;
} | null;

// Warning: (ae-forgotten-export) The symbol "RgbColor" needs to be exported by the entry point index.d.ts
//
// @public
export const hex_to_rgb: (hex: string) => RgbColor;

// @public
export const num_expand: (amount: number | string, unit?: number, accuracy?: number) => string | number;

// @public
export const num_expand_100: (amount: number | string, accuracy?: number) => string | number;

// @public
export const num_reduce_100: (amount: any) => any;

// @public
export const num_text: (number: number | string | undefined, type?: string) => string | false;

// @public
export const reduce_opacity: (rgb: RgbColor, opacity: number) => RgbColor;

// @public
export const rgb_to_hex: (rgb: RgbColor) => string;

// @public
export const sys_detectBrowser: () => void;

// @public
export const sys_print: (props: {
    url: string;
}) => void;

// @public (undocumented)
export const tem_compare_version: (version1: string, version2: string) => boolean;

// Warning: (ae-forgotten-export) The symbol "KeyVal" needs to be exported by the entry point index.d.ts
// Warning: (ae-forgotten-export) The symbol "TableHeaderItem" needs to be exported by the entry point index.d.ts
//
// @public
export const tem_get_tableHeader: (keyVal: KeyVal, cache: TableHeaderItem[], initial: TableHeaderItem[]) => TableHeaderItem[];

// @public
export const timejs: (props?: any, props1?: any, props2?: any) => dayjs.Dayjs;

// @public
export const val_edgeSpace: (value?: any) => boolean;

// @public
export const val_email: (value?: any) => boolean;

// @public
export const val_empty: (value?: any) => boolean;

// @public
export const val_idCard: (value?: any) => boolean;

// @public
export const val_num: (value?: any) => boolean;

// @public
export const val_phone: (value?: any) => boolean;

// @public
export const val_space: (value?: any) => boolean;

// @public
export const win_dynamic_fontSize: () => void;

// Warnings were encountered during analysis:
//
// dist/types/modules/author.d.ts:72:5 - (ae-forgotten-export) The symbol "ResourceList" needs to be exported by the entry point index.d.ts

// (No @packageDocumentation comment for this package)

```
