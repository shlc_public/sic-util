# sic-util

[![NPM version](https://img.shields.io/npm/v/sic-ui.svg?style=flat)](https://npmjs.org/package/sic-ui)
[![NPM downloads](http://img.shields.io/npm/dm/sic-ui.svg?style=flat)](https://npmjs.org/package/sic-ui)

A react library developed with dumi

## Usage

TODO

## Options

TODO
git add .
git commit -m "feat: 更新" --no-verify

https://shlc_public.gitlab.io/sic-util/

## Development

```bash
# install dependencies
$ npm install

# develop library by docs demo
$ npm start

# build library source code
$ npm run build

# build library source code in watch mode
$ npm run build:watch

# build docs
$ npm run docs:build



# check your project for potential problems
$ npm run doctor
```

## LICENSE

MIT
